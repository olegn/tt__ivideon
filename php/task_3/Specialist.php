<?php

namespace Specialist;

/**
 * Хранилище сущности "специалист".
 */
class Specialist
{
    /**
     * Имя специалиста.
     *
     * @var string
     */
    protected $name;

    /**
     * Резюме специалиста.
     *
     * @var string
     */
    protected $cv;

    /**
     * Должность специалиста.
     *
     * @var string
     */
    protected $position;

    /**
     * Дата отправки резюме специалиста.
     *
     * @var string
     */
    protected $dateSendingCv;

    /**
     * Дата приема на работу специалиста.
     *
     * @var string
     */
    protected $dateEmployment;


    /**
     * Поиск специалиста
     * (например, по id).
     *
     * @param integer $id
     * @return array
     */
    public function find(int $id) : bool
    {
        // Подготовить запрос (для примера)
        $sql = 'SELECT * FROM `specialists` WHERE `id`=' . $id;

        // Вернуть признак: найден или нет специалист
        return $this->prepareFinded($sql);
    }

    /**
     * Получить имя специалиста.
     *
     * @return string
     */
    public function getName() : ?string
    {
        return $this->name;
    }

    /**
     * Получить резюме специалиста.
     *
     * @return string
     */
    public function getCv() : ?string
    {
        return $this->cv;
    }

    /**
     * Получить должность специалиста.
     *
     * @return string
     */
    public function getPosition() : ?string
    {
        return $this->position;
    }

    /**
     * Получить дату отправки резюме специалиста.
     *
     * @return string
     */
    public function getDateSendingCv() : ?string
    {
        return $this->dateSendingCv;
    }

    /**
     * Получить дату приема на работу специалиста.
     *
     * @return string
     */
    public function getDateEmployment() : ?string
    {
        return $this->dateEmployment;
    }

    /**
     * Найти запись в базе
     * и подготовить результат.
     *
     * @param string $query
     * @return boolean
     */
    protected function prepareFinded(string $query) : bool
    {
        // Произвести выборку
        // (например, уже существует соединение с БД и есть некая ф-я query)
        $result = query($query);

        if ($result) {
            // Подготовить результат
            $data = fetch($result);

            $this->name = $data['name'] ?: null;
            $this->cv = $data['cv'] ?: null;
            $this->position = $data['position'] ?: null;
            $this->dateSendingCv = $data['dateSendingCv'] ?: null;
            $this->dateEmployment = $data['dateEmployment'] ?: null;
        }

        // Вернуть признак: найден или нет специалист
        return (bool) $result;
    }
}
