<?php

namespace Specialist;

use Specialist\Specialist;

/**
 * Соискатель
 */
class Applicant extends Specialist
{
    /**
     * Можно переписать метод find таким образом,
     * чтобы находил специалистов только на "стадии" соискания.
     *
     * @param integer $id
     * @return array
     */
    public function find(int $id) : bool
    {
        // Подготовить запрос (для примера)
        $sql = 'SELECT * FROM `specialists` WHERE `id`=' . $id . ' AND ``=';

        // Вернуть признак: найден или нет специалист
        return $this->prepareFinded($sql);
    }

    /**
     * Создать специалиста-соискателя.
     *
     * @return void
     */
    public function create() : void
    {
        //
    }

    /**
     * Принять на работу специалиста-соискателя.
     *
     * @return void
     */
    public function recruit() : void
    {
        //
    }
}
