<?php

/**
 * Проверить дату на соответствие формату
 *
 * @param string $format    - формат даты
 * @param string $time      - дата в виде строки
 * @return boolean
 */
function checkDateWithFormat(string $format, string $time) : bool {
    return DateTime::createFromFormat($format, $time) === false
        ? false
        : true;
}
