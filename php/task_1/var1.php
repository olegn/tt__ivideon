<?php

class Builder implements ArrayAccess
{
    private $container = array();

    /**
     * Сохранение результата.
     *
     * @return void
     */
    public function save() : void
    {
        // Обработка сохранения результата
    }

    /**
     * Определяет, существует ли заданное смещение.
     *
     * @param mixed $offset
     * @return boolean
     */
    public function offsetExists($offset) : bool
    {
        return isset($this->container[$offset]);
    }

    /**
     * Возвращает заданное смещение (ключ).
     *
     * @param mixed $offset
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Присваивает значение заданному смещению.
     *
     * @param mixed $offset
     * @param mixed $value
     * @return void
     */
    public function offsetSet($offset, $value) : void
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Удаляет смещение.
     *
     * @param mixed $offset
     * @return void
     */
    public function offsetUnset($offset) : void
    {
        unset($this->container[$offset]);
    }
}
