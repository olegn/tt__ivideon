/*
id int not null primary key
name text
begin_date datetime // дата начала события
end_date datetime // дата окончания события
*/

SELECT
    `id`,
    `name`
FROM
    `events`
WHERE
    (
        YEAR(`begin_date`) = YEAR(NOW())
    AND
        WEEK(`begin_date`, 1) = WEEK(NOW(), 1)
    )
OR
    (
        YEAR(`end_date`) = YEAR(NOW())
    AND
        WEEK(`end_date`, 1) = WEEK(NOW(), 1)
    )
