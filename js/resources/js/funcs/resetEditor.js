/**
 * Очистить редактор.
 */
function resetEditor() {
    if (isIDE) {
        editor.setValue('');
    } else {
        editor.value = '';
    }
}
