/**
 * Создание текстовых стилей в head в тэге style.
 *
 * @param string styles
 */
function addStyle(styles, context) {
    context = context || document;
    var css = context.createElement('style');
    css.type = 'text/css';

    if (css.styleSheet) {
        css.styleSheet.cssText = styles;
    } else {
        css.appendChild(context.createTextNode(styles));
    }

    context.getElementsByTagName('head')[0].appendChild(css);
}
