/**
 * Ввод данных в редактор.
 * В идеале, необходимо повесить setTimeout
 * и через него вызывать эту функцию,
 * если вообще оставлять автообновление
 * (хотя, это не было предусмотрено заданием)
 */
function changeEditor() {
    var val = 'getValue' in editor ? editor.getValue() : editor.value;

    // создать кэш для css
    if (!('_css' in assets)) {
        assets._css = [];
    }

    // создать кэш для js
    if (!('_js' in assets)) {
        assets._js = [];
    }

    // Подготовить и добавить в кэш css тэги
    for (var i = 0, c = assets.css.length; i < c; i++) {
        assets._css[i] = '<link rel="stylesheet" href="' + assets.css[i] + '">';
    }

    // Подготовить и добавить в кэш js тэги
    for (var i = 0, c = assets.js.length; i < c; i++) {
        assets._js[i] = '<script src="' + assets.js[i] + '"><script>';
    }

    // Если пользователь начал не с объявления типа документа,
    // то введенные данные расценивать как чистый html без базовых тэгов
    if (!/^\<\!doctype/i.test(val.trim())) {
        template = template.replace('{{head}}', assets._css.concat(assets._js).join(''));
        val = template.replace('{{body}}', val);
    }

    frameDoc.open('text/html', 'replace');
    frameDoc.write(val);
    frameDoc.close();

    var imgColl = frameDoc.body.querySelectorAll('img');
    for (var i = 0, c = imgColl.length; i < c; i++) {
        var newImgWrap = document.createElement('div');
        newImgWrap.className = 'b-image';
        newImgWrap.appendChild(imgColl[i].cloneNode(1));

        if (imgColl[i].width) {
            newImgWrap.style.width = (imgColl[i].width + 4) + 'px';
        }

        if (imgColl[i].alt) {
            var newImgCaption = document.createElement('p');
            newImgCaption.appendChild(document.createTextNode(imgColl[i].alt));
            newImgWrap.appendChild(newImgCaption);
        }

        imgColl[i].parentNode.replaceChild(newImgWrap, imgColl[i]);
    }

    addStyle(styles, frameDoc)
}
