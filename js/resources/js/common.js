document.addEventListener('DOMContentLoaded', function () {
    editor = document.getElementById('editor');
    var oldVal = editor.value;
    var result = document.getElementById('result');
    var reset = document.getElementById('reset');
    var apply = document.getElementById('apply');

    // Создать iFrame
    var newIframe = document.createElement('iframe');
    newIframe.classList.add('main__frame');
    // Сделать iFrame "дружелюбным"
    newIframe.src = 'about:blank';
    // Кое-какие настройки
    newIframe.setAttribute('allowtransparency', true);
    newIframe.setAttribute('frameborder', '0');
    // Поместить в дом
    result.appendChild(newIframe);

    frameDoc = newIframe.contentWindow.document;

    try {
        var newEditor = document.createElement('div');
        newEditor.classList.add(editor.className);
        newEditor.id = 'editor';
        editor.parentNode.replaceChild(newEditor, editor);
        editor = ace.edit('editor');
        editor.setTheme('ace/theme/monokai');
        editor.session.setMode('ace/mode/html');
        editor.session.on('change', changeEditor);
        editor.setValue(oldVal.trim().replace(/\n[\t\s]+/ig, '\n'));
        editor.focus();
        isIDE = true;
    } catch (err) {
        editor.addEventListener('input', changeEditor, false);
    }

    reset.addEventListener('click', resetEditor, false);
    apply.addEventListener('click', changeEditor, false);
}, false);

/**
 * Слушать экран на соответствие медиа запросу
 */
screenTest.addListener(function (x) {
    if (x.matches) {
        // Desktop
        console.log(window.location.hash)
        if (window.location.hash) {
            window.location.hash = '';
        }
    } else {
        // Mobile
    }
});
