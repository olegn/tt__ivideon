// Глобальные переменные

var desktopScreen = 864;
var screenTest = window.matchMedia('(min-width: ' + (desktopScreen) + 'px)');

var editor;
var frameDoc;
var template = '<!DOCTYPE html><html><head><title></title>{{head}}</head><body>{{body}}</body></html>';
var assets = {
    css: [],
    js: []
}
var isIDE = false;
var styles = '\
    .b-image {\
        position: relative;\
        display: inline-block;\
        border: px2em(1) solid rgba(0, 0, 0, .12);\
        border-radius: .3125em;\
        background-color: #eee;\
        overflow: hidden;\
    }\
\
    .b-image img {\
        position: relative;\
        display: block;\
        border-radius: .25em;\
        margin: .125em auto 0 auto;\
    }\
\
    .b-image p {\
        position: relative;\
        display: block;\
        padding: .5em 1em;\
        margin: 0;\
        line-height: 1.25em;\
        color: rgba(0, 0, 0, 64);\
        text-align: center;\
    }\
';
